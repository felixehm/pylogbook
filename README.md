PyLogbook is a Python wrapper for the eLogbook Java API.

Full documentation and examples are available on the Scripting Tools wiki:
https://wikis.cern.ch/display/ST/PyLogbook
