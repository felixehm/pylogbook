#!/usr/bin/env python
# -*- coding: utf-8 -*-

import ast
import os
import setuptools

from setuptools.command.install import install as _install


def get_version_from_init():
    init_file = os.path.join(
        os.path.dirname(__file__), 'pylogbook', '__init__.py'
    )
    with open(init_file, 'r') as fd:
        for line in fd:
            if line.startswith('__version__'):
                return ast.literal_eval(line.split('=', 1)[1].strip())


# Custom install function to install and register with cmmnbuild-dep-manager
class install(_install):
    '''Install and perform the jar resolution'''
    user_options = _install.user_options + [
        ('no-jars', None, 'do not register with cmmnbuild_dep_manager')
    ]

    def initialize_options(self):
        self.no_jars = False
        _install.initialize_options(self)

    def run(self):
        if not self.no_jars:
            import cmmnbuild_dep_manager
            mgr = cmmnbuild_dep_manager.Manager()
            mgr.install('pylogbook')
            print('registered pylogbook with cmmnbuild_dep_manager')
        _install.run(self)


setuptools.setup(
    name='pylogbook',
    version=get_version_from_init(),
    description='A Python wrapping of the CERN eLogbook API',
    author='Spencer Gessner',
    author_email='spencer.j.gessner@cern.ch',
    url='https://gitlab.cern.ch/scripting-tools/pylogbook',
    packages=['pylogbook'],
    install_requires=[
        'JPype1>=0.6.1',
        'cmmnbuild-dep-manager>=2.1.0'
    ],
    cmdclass={
        'install': install
    }
)
