# -*- coding: utf-8 -*-
"""A Python wrapping of the CERN eLogbook API

Copyright (c) CERN 2017

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

:Authors:
  - Spencer Gessner <spencer.j.gessner@cern.ch>
  - Tom Levens <tom.levens@cern.ch>
"""

import cmmnbuild_dep_manager
import jpype


class eLogbook(object):
    def __init__(self,logbook):
        self.mgr = cmmnbuild_dep_manager.Manager('pylogbook')
        self.mgr.start_jpype_jvm()
        cern = jpype.JPackage('cern')
        self._ELBExternalSources = cern.op.elogbook.client.ELBExternalSources
        self.logbook = logbook

    def create_event(self, comment):
        return eLogbookEvent(self, comment)

class eLogbookEvent(object):
    def __init__(self, parent, comment):
        self._ELBExternalSources = parent._ELBExternalSources
        self.event = self._ELBExternalSources.addEvent(parent.logbook, comment)

    def add_attachment(self, filename, comment):
        j_file = jpype.java.io.File(filename)
        self._ELBExternalSources.updEventAddAttachment(self.event, comment, j_file)

    @property
    def comment(self):
        return self._ELBExternalSources.eventGetComment(self.event)

    @comment.setter
    def comment(self, value):
        self._ELBExternalSources.updEventComment(self.event, value)
